const unique = require("../src/second_task").unique;
const expect = require('chai').expect;

describe('second', () => {
    it('решение 2 задачи корректно', () => {
      expect(unique('alpha beta beta gamma gamma gamma delta alpha beta beta gamma gamma gamma delta')).equal('alpha beta gamma delta');
      expect(unique('rJVTorVMq rJVTorVMq rJVTorVMq gdueJHoWr gdueJHoWr gdueJHoWr gdueJHoWr x gdueJHoWr x gdueJHoWr rJVTorVMq gdueJHoWr gdueJHoWr')).equal('rJVTorVMq gdueJHoWr x');
      expect(unique('n n n n cRKAbhu X X X cRKAbhu cRKAbhu cRKAbhu n LnqDCtJsN n LnqDCtJsN X n cRKAbhu LnqDCtJsN X cRKAbhu n n X cRKAbhu X X LnqDCtJsN X X LnqDCtJsN')).equal('n cRKAbhu X LnqDCtJsN');
      expect(unique('MmahA symFm MmahA npRZqZe MmahA symFm MmahA symFm RhZ symFm MmahA MmahA symFm RhZ RhZ RhZ MmahA RhZ MmahA MmahA MmahA')).equal('MmahA symFm npRZqZe RhZ');
    });
  });