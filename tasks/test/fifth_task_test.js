const minMaxValues = require("../src/fifth_task").minMaxValues;
const expect = require('chai').expect;

describe('fifth', () => {
    it('решение 5 задачи корректно', () => {
      expect(minMaxValues("4 5 29 54 4 0 -214 542 -64 1 -3 6 -6")).equal("542 -214");
      expect(minMaxValues("10 2 -2 -10")).equal("10 -10");
      expect(minMaxValues("1 -1")).equal("1 -1");
      expect(minMaxValues("42")).equal("42 42");
      expect(minMaxValues("-1 -1 0")).equal("0 -1");
      expect(minMaxValues("1 1 0")).equal("1 0");
    });
  });