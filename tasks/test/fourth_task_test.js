const countElem = require("../src/fourth_task").countElem;
const expect = require('chai').expect;

describe('fourth', () => {
    it('решение 4 задачи корректно', () => {
      expect(countElem(['Perl','Closure','JavaScript'],['Go', 'C++','Erlang'])).equal(0);
      expect(countElem(['incapsulation','OOP','array'],['time', 'propert','paralelism','OOP'])).equal(1);
      expect(countElem([1,2,3,4,5],[2,3,4,5,6])).equal(4);
      expect(countElem([],[])).equal(0);
      expect(countElem([1],[])).equal(0);
      expect(countElem([],[1])).equal(0);
      expect(countElem([1],[1])).equal(1);
      expect(countElem([1],[true])).equal(0);
      expect(countElem([1],["1"])).equal(0);
      expect(countElem([ true, 3, 9, 11, 15 ], [ true, 3, 11 ])).equal(3);
    });
  });