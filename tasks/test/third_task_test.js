const isProgrammerDay = require("../src/third_task").isProgrammerDay;
const expect = require('chai').expect;
describe('third', () => {
    it('решение 3 задачи корректно', () => {
      expect(isProgrammerDay(new Date(2019, 0, 0))).equal(false);
      expect(isProgrammerDay(new Date(2012, 2, 14))).equal(false);
      expect(isProgrammerDay(new Date(2020, 5, 15))).equal(false);
      expect(isProgrammerDay(new Date(2019, 11, 31))).equal(false);
      expect(isProgrammerDay(new Date(1980, 1, 14))).equal(true);
      expect(isProgrammerDay(new Date(2019, 1, 14))).equal(true);
      expect(isProgrammerDay(new Date(2050, 1, 14))).equal(true);
    });
  });