const converted = require("../src/first_task").convert;
const expect = require('chai').expect;

describe('first_task', () => {
    it('решение 1 задачи корректно', () => {
      expect(converted("ZpglnRxqenU")).equal("Z-Pp-Ggg-Llll-Nnnnn-Rrrrrr-Xxxxxxx-Qqqqqqqq-Eeeeeeeee-Nnnnnnnnnn-Uuuuuuuuuuu");
      expect(converted("EvidjUnokmM")).equal("E-Vv-Iii-Dddd-Jjjjj-Uuuuuu-Nnnnnnn-Oooooooo-Kkkkkkkkk-Mmmmmmmmmm-Mmmmmmmmmmm");
      expect(converted("a")).equal("A");
      expect(converted("abc")).equal("A-Bb-Ccc");
    });
  });
