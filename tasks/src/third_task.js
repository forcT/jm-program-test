"use strict";
//@ts-check

//for test
//let new_m = 0;
    // if(prog_date.getMonth() === 1 || prog_date.getMonth() === 2) {
    //     new_m+=prog_date.getMonth()+12;
    //     prog_date.setMonth(new_m);
    //     prog_date.setFullYear(prog_date.getFullYear() - 1);
    // }   
    // let res = parseInt((prog_date.getDay() + ((13 * (prog_date.getMonth() + 1)) / 5 ) 
    // + prog_date.getFullYear() + (prog_date.getFullYear() / 4) - (prog_date.getFullYear() / 100) + (prog_date.getFullYear() / 400)) % 7);

/**
 * Задача 3
* На входе функции дата (объект Date)
* Функция должна вернуть true если это день компьютерщика (14 февраля)

* Пример:
* isProgrammerDay(new Date(2013, 5, 24))  // false
* isProgrammerDay(new Date(2013, 1, 14))   // true
* isProgrammerDay(new Date(3000, 11, 24))  // false
* isProgrammerDay(new Date(3000, 1, 14))  // false
* @function isProgrammerDay
* @param {Object} str_date - a passed date
* @returns {Boolean} - true/false 
*/
function isProgrammerDay(str_date) {
    let prog_date = new Date('1946-02-14');
    //return str_date.getMonth() === prog_date.getMonth() && str_date.getDay() === prog_date.getDay() ? true:false;
    return str_date.getMonth() === prog_date.getMonth() && str_date.getDate() === prog_date.getDate() ? true:false;
}

module.exports.isProgrammerDay = isProgrammerDay;