"use strict";
//@ts-check
const performance = require('perf_hooks').performance;

/**
 * This is a desctiption of the converted_str function:
 * На входе функции строка со словами, разделенными пробелом
 * Нужно вернуть строку, где остались только уникальные (не повторяюещеся) слова в порядке их появления
 * Пример: var srt = 'alpha beta beta gamma gamma gamma delta alpha beta beta gamma gamma gamma delta';
 * unique(str); // 'alpha beta gamma delta'
 * @function unique
 * @param {string} str - an input string value
 * @return {string} - an output string value
 */

let time_one = performance.now();
function unique(str) {
    //convert our input string value to array
    if(isEmpty(str) === false) {
        console.error("Your passed string is empty. I need not empty one!");
    }else {
        //I have created two algorithms to find all unique values and chose the better one
        return [...new Set(str.replace(/\s/g, ',').split(','))].toString().replace(/,/g,' ');
        //return str.replace(/\s/g, ',').split(',').filter((value,index,array) => array.indexOf(value) === index).toString().replace(/,/g,' ');
    }
}
time_one = performance.now() - time_one;

/**
 * This is description of the isEmpty function
 * We need to check out if a passed string is empty or not
 * @function isEmpty
 * @param {boolean} boolean_result - false if a string is empty, true - if not
 */
function isEmpty(str) {
  return !str ? false : true;
}

// /**
//  * This function is used whenever spaces are needed to be deleted(changed to ',') in a string
//  * @function change_space
//  * @param {string} str - an input string value
//  * @returns {string} - string without 
//  */
// function change_space(str) {
//     return str.replace(/\s/g, ',').split(',');
// }

module.exports.unique = unique;

console.log(unique('alpha beta beta gamma gamma gamma delta alpha beta beta gamma gamma gamma delta'));
console.log(`Execution time is ${time_one}`);