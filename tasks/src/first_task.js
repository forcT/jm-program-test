"use strict";
//@ts-check
const performance = require('perf_hooks').performance;

/**
 * This is a desctiption of the converted_str function:
 * Функция принимает строку
 * На выходе должна быть строка в соответствии со следюущим примером:
 * convert("abcd"); // "A-Bb-Ccc-Dddd"
 * convert("RqaEzty"); // "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
 * convert("cwAt"); // "C-Ww-Aaa-Tttt"
 * @function convert
 * @param {string} str - an input string
 * @returns {converted_str} - the returned function
 */
function convert(str) {
  //check if an input string hasn't been passed
  if (isEmpty(str) === false) {
    console.error("Your passed string is empty. I need not empty one!");
  } else return converted_func_two(str);
}

/**
 * This is description of the isEmpty function
 * We need to check out if a passed string is empty or not
 * @function isEmpty
 * @param {boolean} boolean_result - false if a string is empty, true - if not
 */
function isEmpty(str) {
  return !str ? false : true;
}

let time_one = performance.now();
/**
 * @function converted_func_one
 * @param {string} str - an input string
 * @returns {string} str - an output converted string
 */
function converted_func_one(str) {
    let converted_str = '';
    for(let i = 0; i < str.length; i++) {
        converted_str += str.trim()[i].toLocaleUpperCase() + str.trim()[i].toLocaleLowerCase().repeat(i) + '-';
    }
    return converted_str.slice(0,converted_str.length-1)+converted_str.slice(converted_str.length);
}
time_one = performance.now() - time_one;

let time_two = performance.now();
/**
 * This function like the same one above(created for test)
* @function converted_func_two
 * @param {string} str - an input string
 * @returns {string} str - an output converted string
 */
function converted_func_two(str) {
    let converted_str = '';
    for(let i = 0; i < str.length; i++) {
        if(str[i] === str[i].toLocaleLowerCase()){
            converted_str += str.trim()[i].toLocaleUpperCase() + str.trim()[i].toLocaleLowerCase().repeat(i) + '-';
        }else {
            converted_str += str.trim()[i] + str.trim()[i].toLocaleLowerCase().repeat(i) + '-';
        }
    }
    return converted_str.slice(0,converted_str.length-1)+converted_str.slice(converted_str.length);
}
time_two = performance.now() - time_two;

//before test
// console.log(convert("abcd"));
// console.log(convert("RqaEzty"));
// console.log(convert("cwAt"));
// console.log(convert("ZpglnRxqenU"));

//I have created this part of code to test execution time each of my converted function and choose the best one(converted_func_two is better than converted_func_one)
// console.log(`Execution time of the converted_func_one is ${time_one}`);
// console.log(`Execution time of the converted_func_two is ${time_two}`);

module.exports.convert = convert;
