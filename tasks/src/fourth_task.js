"use strict";
//@ts-check


/**This function receives two arrays and returns the number of the repeatable elements which are in these ones
 * @function countElem
 * @param {Object []} arrOne - the first passed array
 * @param {Object []} arrTwo - the second one
 * @returns {number} - the number of the repeatable elements
 */
function countElem(arrOne, arrTwo) {
    let concatedArr = arrOne.concat(arrTwo);
    if(isEmpty(arrOne,arrTwo)){
        console.error('Not two arguments have been passed!');
    }else {
        if(!isArray(arrOne,arrTwo)){
            console.log('Not array arguments have been passed!');
        } else{
            return concatedArr.filter((value,index,array)=>array.indexOf(value)!==index).length;
        }
    }
}

/**
 * @function isEmpty
 * @param {Object []} argOne - the first passed array
 * @param {Object []} argTwo - the second one
 * @returns {boolean} - true/false depends on a result
 */
function isEmpty(argOne, argTwo) {
    return !argOne || !argTwo ? true : false;
}

/**
 * @function isArray
 * @param {Object []} argOne - the first passed array
 * @param {Object []} argTwo - the second one
 * @returns {boolean} - true/false depends on a result
 */
function isArray(argOne, argTwo) {
    return toString.call(argOne) === '[object Array]' && toString.call(argTwo) === '[object Array]' ? true : false;
}

console.log(countElem(['Erlang', 'JavaScript'],['Go', 'C++', 'Erlang']));

module.exports.countElem = countElem;