"use strict";
//@ts-check

/**
 * This function returns min and max values of a passed string
 * @function minMaxValues
 * @param {string} str - an input values
 * @returns {string} - contains min and max values 
 */
function minMaxValues(str) {
    let min, max = 0;
    if(!isString(str)) console.error('No string has been passed!');
    else {
        min = Math.min.apply(null,str.replace(/\s/g,',').split(','));
        max = Math.max.apply(null,str.replace(/\s/g,',').split(','));
    }
    return `${max} ${min}`;
}

/**
 * @function isString
 * @param {string} str - an input string
 * @return {boolean} - true/false depends on result
 */
function isString(str) {
    return toString.call(str) === '[object String]' ? true : false;
}

console.log(minMaxValues('1 2 3 4'));

module.exports.minMaxValues = minMaxValues;